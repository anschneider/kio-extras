# translation of kio_man.po to Frysk
#
# Rinse de Vries <rinsedevries@kde.nl>, 2005, 2006, 2007, 2009.
msgid ""
msgstr ""
"Project-Id-Version: kio_man\n"
"Report-Msgid-Bugs-To: https://bugs.kde.org\n"
"POT-Creation-Date: 2022-10-10 00:45+0000\n"
"PO-Revision-Date: 2009-08-15 22:30+0100\n"
"Last-Translator: Berend Ytsma <berendy@gmail.com>\n"
"Language-Team: Frysk <kde-i18n-fry@kde.org>\n"
"Language: fy\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: KBabel 1.11.4\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"

#: kio_man.cpp:451
#, fuzzy, kde-kuit-format
#| msgid ""
#| "No man page matching to %1 found.<br /><br />Check that you have not "
#| "mistyped the name of the page that you want.<br />Check that you have "
#| "typed the name using the correct upper and lower case characters.<br />If "
#| "everything looks correct, then you may need to improve the search path "
#| "for man pages; either using the environment variable MANPATH or using a "
#| "matching file in the /etc directory."
msgctxt "@info"
msgid ""
"No man page matching <resource>%1</resource> could be found.<nl/><nl/>Check "
"that you have not mistyped the name of the page, and note that man page "
"names are case sensitive.<nl/><nl/>If the name is correct, then you may need "
"to extend the search path for man pages, either using the <envar>MANPATH</"
"envar> environment variable or a configuration file in the <filename>/etc</"
"filename> directory."
msgstr ""
"Gjin man-side fûn dy oerienkomt mei %1. <br /><br />Kontrolearje of jo de "
"namme fan de side dy jo wolle goed skreaun hawwe.<br />Let goed op it juste "
"gebrûk fan haad-/lytse letters! <br />As alles der goed útsjocht, dan kin it "
"wêze dat it sykpaad foar de man-siden net goed ynsteld is. Stel dan in "
"better paad yn troch middel fan de omjouwingsfariabele 'MANPATH' of in "
"oerienkommende triem út de map /etc."

#: kio_man.cpp:569
#, kde-kuit-format
msgctxt "@info"
msgid ""
"The specified man page references another page <filename>%1</filename>,<nl/"
">but the referenced page <filename>%2</filename> could not be found."
msgstr ""

#: kio_man.cpp:586
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be read."
msgstr ""

#: kio_man.cpp:595
#, kde-kuit-format
msgctxt "@info"
msgid "The man page <filename>%1</filename> could not be converted."
msgstr ""

#: kio_man.cpp:660
#, fuzzy, kde-format
#| msgid "<h1>KDE Man Viewer Error</h1>"
msgid "Manual Page Viewer Error"
msgstr "<h1>Flater yn KDE Man besjogger</h1>"

#: kio_man.cpp:674
#, fuzzy, kde-format
#| msgid "There is more than one matching man page."
msgid "There is more than one matching man page:"
msgstr "Der binne meardere mansiden dy oerienkomme."

#: kio_man.cpp:674
#, kde-format
msgid "Multiple Manual Pages"
msgstr ""

#: kio_man.cpp:686
#, kde-format
msgid ""
"Note: if you read a man page in your language, be aware it can contain some "
"mistakes or be obsolete. In case of doubt, you should have a look at the "
"English version."
msgstr ""
"Taljochting: as jo in man-side yn jo eigen taal lêze, hâld der dan rekken "
"mei dat dizze flater befetsje ki of efterhelle is. Yn gefal fan ûnwissens: "
"besjoch de ingelske ferzje."

#: kio_man.cpp:758
#, kde-format
msgid "Header Files"
msgstr ""

#: kio_man.cpp:759
#, kde-format
msgid "Header Files (POSIX)"
msgstr ""

#: kio_man.cpp:760
#, kde-format
msgid "User Commands"
msgstr "brûkerskommando's"

#: kio_man.cpp:761
#, fuzzy, kde-format
#| msgid "User Commands"
msgid "User Commands (POSIX)"
msgstr "brûkerskommando's"

#: kio_man.cpp:762
#, kde-format
msgid "System Calls"
msgstr "Systeemoanroppen"

#: kio_man.cpp:763
#, kde-format
msgid "Subroutines"
msgstr "Subroutines"

#: kio_man.cpp:765
#, kde-format
msgid "Perl Modules"
msgstr "Perl-modules"

#: kio_man.cpp:766
#, kde-format
msgid "Network Functions"
msgstr "Netwurkfunksjes"

#: kio_man.cpp:767
#, kde-format
msgid "Devices"
msgstr "Apparaten"

#: kio_man.cpp:768
#, kde-format
msgid "File Formats"
msgstr "triemformaten"

#: kio_man.cpp:769
#, kde-format
msgid "Games"
msgstr "Spultsjes"

#: kio_man.cpp:770
#, kde-format
msgid "Miscellaneous"
msgstr "Ferskate"

#: kio_man.cpp:771
#, kde-format
msgid "System Administration"
msgstr "Systeemadministraasje"

#: kio_man.cpp:772
#, kde-format
msgid "Kernel"
msgstr "Kernel"

#: kio_man.cpp:773
#, kde-format
msgid "Local Documentation"
msgstr "Lokale dokumintaasje"

#: kio_man.cpp:775
#, kde-format
msgid "New"
msgstr "Nij"

#: kio_man.cpp:805
#, fuzzy, kde-format
#| msgid "UNIX Manual Index"
msgid "Main Manual Page Index"
msgstr "UNIX Manual Indeks"

#: kio_man.cpp:830
#, kde-format
msgid "Section %1"
msgstr "Seksje %1"

#: kio_man.cpp:1115
#, fuzzy, kde-format
#| msgid "Index for Section %1: %2"
msgid "Index for section %1: %2"
msgstr "Indeks foar seksje %1: %2"

#: kio_man.cpp:1115
#, fuzzy, kde-format
#| msgid "UNIX Manual Index"
msgid "Manual Page Index"
msgstr "UNIX Manual Indeks"

#: kio_man.cpp:1127
#, kde-format
msgid "Generating Index"
msgstr "Dwaande de Yndeks oan te meitsjen"

#: kio_man.cpp:1405
#, fuzzy, kde-kuit-format
#| msgid ""
#| "Could not find the sgml2roff program on your system. Please install it, "
#| "if necessary, and extend the search path by adjusting the environment "
#| "variable PATH before starting KDE."
msgctxt "@info"
msgid ""
"Could not find the <command>%1</command> program on your system. Please "
"install it if necessary, and ensure that it can be found using the "
"environment variable <envar>PATH</envar>."
msgstr ""
"It programma sgml2groff is net fûn op jo systeem. ynstallearje it, at it "
"nedich is, en wreidzje it sykpaad út troch omjouwingsfariabele \"PATH\" oan "
"te passen foardat jo KDE begjinne."

#~ msgid "Open of %1 failed."
#~ msgstr "It iepenjen fan %1 is mislearre."

#~ msgid "Man output"
#~ msgstr "Man-útfier"

#~ msgctxt "NAME OF TRANSLATORS"
#~ msgid "Your names"
#~ msgstr "Berend Ytsma"

#~ msgctxt "EMAIL OF TRANSLATORS"
#~ msgid "Your emails"
#~ msgstr "berendy@bigfoot.com"

#~ msgid "KMan"
#~ msgstr "KMan"
